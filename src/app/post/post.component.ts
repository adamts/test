import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();

 
  

  post:Post;
  tempPost:Post = {title:null};



 

  isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }



  sendDelete(){

   this.deleteEvent.emit(this.post);
  }

  cancel(){
    this.isEdit = false;
    this.post.title = this.tempPost.title;
   
    this.editButtonText = 'Edit'; 
  }

  


  toggleEdit(){
     //update parent about the change
    this.isEdit = !this.isEdit; 

     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     
     if(this.isEdit){ 
       this.tempPost.title = this.post.title;
     } else {     
       this.editEvent.emit(this.post);
     }
    
  }

  ngOnInit() {
  }

}