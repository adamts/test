import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {

  //private _url = 'http://jsonplaceholder.typicode.com/posts';
  postsObservable;

  constructor(private af:AngularFire) { }

  addPost(post){
    this.postsObservable.push(post);
  }

  deletePost1(post){
    this.af.database.object('/posts/' + post.$key).remove();
    console.log('/post/' + post.$key);
  }

  updatePost(post){
    let post1 = {title:post.title}
    console.log(post1);
    this.af.database.object('/posts/' + post.$key).update(post1)
  }  


  getPosts(){
this.postsObservable = this.af.database.list('/posts');
    return this.postsObservable;
	}

  
   
  
 

 

}