import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {

 posts;

 currentPost;

 isLoading = true;

 constructor(private _postsService: PostsService) {
    //this.posts = this._postService.getPosts();
  }

 
 select(post){
		this.currentPost = post; 
   // console.log(	this.currentPost);
 }

 deletePost(post){
    this._postsService.deletePost1(post);
  }

   addPost(post){

    this._postsService.addPost(post);
  }


    editPost(post){
     this._postsService.updatePost(post); 
  }  



   
  

  ngOnInit() {
        this._postsService.getPosts()
			    .subscribe(posts => {this.posts = posts;
                               this.isLoading = false;
                                 console.log(posts)});
                        
                              
                           
  }

  

  

}