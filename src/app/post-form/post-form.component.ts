import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post'

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
 @Output() postAddedEvent = new EventEmitter<Post>();

 post:Post = {
   title: ''
  
     };


  constructor() { }
  

  onSubmit(form:NgForm){
    console.log(form);
    this.postAddedEvent.emit(this.post);
    this.post = {

     title: ''
   

    }
  }



  ngOnInit() {
  }

}