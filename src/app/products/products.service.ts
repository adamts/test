import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {

    productsObservable;

   constructor(private af:AngularFire) { }

   deleteProduct1(product){
    this.af.database.object('/products/' + product.$key).remove();
    console.log('/product/' + product.$key);
  }

   // getProducts(){
//this.productsObservable = this.af.database.list('/products');
   // return this.productsObservable;
	//}   

     getProducts(){
this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.productCategory = [];
            for(var p in product.category){
                product.productCategory.push(
                this.af.database.object('/category/' + p)
              )
            }
          }
        );
        return products;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.productsObservable;
	}



}
