import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: [`
    .products li { cursor: default; }
    .products li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]

})
export class ProductsComponent implements OnInit {

  products;

   currentPost;




 constructor(private _productsService: ProductsService) {
    //this.posts = this._postService.getPosts();
  }

  deleteProduct(product){
    this._productsService.deleteProduct1(product);
  }

   ngOnInit() {
        this._productsService.getProducts()
			    .subscribe(products => {this.products = products;
                                 console.log(products)});
                        
                              
                           
  }

}

