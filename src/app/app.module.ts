import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import {PostsService} from './posts/posts.service';
import {ProductsService} from './products/products.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';
import{AngularFireModule} from 'angularfire2';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';



export const firebaseConfig = {
      apiKey: "AIzaSyBAgGRZfGPGnne4QnDN8JaFP62zdn87Yvs",
    authDomain: "adam1-ff7a3.firebaseapp.com",
    databaseURL: "https://adam1-ff7a3.firebaseio.com",
    storageBucket: "adam1-ff7a3.appspot.com",
    messagingSenderId: "451789403610"
}



const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'products', component: ProductsComponent },
  { path: '', component: ProductsComponent },
  { path: '**', component: PageNotFoundComponent }
];






@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent,
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],

  
  providers: [PostsService,ProductsService],
  bootstrap: [AppComponent]

})
export class AppModule { }
