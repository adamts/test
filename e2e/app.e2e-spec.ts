import { AdamPage } from './app.po';

describe('adam App', function() {
  let page: AdamPage;

  beforeEach(() => {
    page = new AdamPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
